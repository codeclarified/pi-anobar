var app = require('http').createServer(handler)
	, io = require('socket.io').listen(app)
	, fs = require('fs')
	, child = require('child_process')
	, running = false
	, pidof = require('pidof')
	, ps
	, sys = require('sys')
	, logfile = '/home/pi/pianolog'
	, tail = child.spawn("tail", ["-f", logfile]);



	app.listen(8080);
	myFile = fs.createWriteStream(logfile, {encoding: 'utf8'});

pidof('pianobar', function (err, pid) {
	if (err) {
			console.log('Weird error getting PIDs');
			console.log(err);
	} else {
			if (pid) {
					console.log('Found pianobar at pid ' + pid);
					running = true;
			} else {
					console.log('Seems like there\'s no cron on this system');
					ps = child.spawn('pianobar');
					ps.stdout.setEncoding('utf8');

					ps.stdout.pipe(myFile);
			}
	}
})

function handler (req, res) {
	fs.readFile(__dirname + '/index.html',
	function (err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading index.html');
		}
		res.writeHead(200);
		res.end(data);
	});
}

io.sockets.on('connection', function (socket) {
	socket.emit('news', { hello: 'world' });
	socket.on('my other event', function (data) {
		console.log(data);
	});
	socket.on('play', function (data) {
		socket.emit('msg', { msg: 'Playing' });

		// Open and append command to FIFO
		fs.open("/home/pi/.config/pianobar/ctl", 'a', 0666, function(err, fd){
			fs.write(fd, "p\n", null, undefined, function (err, written) {
				console.log('bytes written: ' + written);
			});
		});
	});
	socket.on('next', function (data) {
		socket.emit('msg', { msg: 'Playing' });

		// Open and append command to FIFO
		fs.open("/home/pi/.config/pianobar/ctl", 'a', 0666, function(err, fd){
			fs.write(fd, "n\n", null, undefined, function (err, written) {
				console.log('bytes written: ' + written);
			});
		});
	});
	socket.on('volup', function (data) {
		socket.emit('msg', { msg: 'Volume up' });

		// Open and append command to FIFO
		fs.open("/home/pi/.config/pianobar/ctl", 'a', 0666, function(err, fd){
			fs.write(fd, ")\n", null, undefined, function (err, written) {
				console.log('bytes written: ' + written);
			});
		});
	});
	socket.on('voldown', function (data) {
		socket.emit('msg', { msg: 'Volume down' });

		// Open and append command to FIFO
		fs.open("/home/pi/.config/pianobar/ctl", 'a', 0666, function(err, fd){
			fs.write(fd, "(\n", null, undefined, function (err, written) {
				console.log('bytes written: ' + written);
			});
		});
	});

	tail.stdout.on("data", function (data) {
		sys.puts(data);
		socket.emit('msg', data);
	});
});